#ifndef STRUCT_DECL_H
#define STRUCT_DECL_H

#define GEN_STRUCT1(NAME,ARG1) \
typedef struct NAME \
{ \
    ARG1; \
} NAME;

#define GEN_STRUCT2(NAME,ARG1,ARG2) \
typedef struct NAME \
{ \
    ARG1; \
    ARG2; \
} NAME;

#define GEN_STRUCT3(NAME,ARG1,ARG2,ARG3) \
typedef struct NAME \
{ \
    ARG1; \
    ARG2; \
    ARG3; \
} NAME;

#define GEN_STRUCT4(NAME,ARG1,ARG2,ARG3,ARG4) \
typedef struct NAME \
{ \
    ARG1; \
    ARG2; \
    ARG3; \
    ARG4; \
} NAME;

#define GEN_STRUCT5(NAME,ARG1,ARG2,ARG3,ARG4,ARG5) \
typedef struct NAME \
{ \
    ARG1; \
    ARG2; \
    ARG3; \
    ARG4; \
    ARG5; \
} NAME;

#define GET_GEN_STRUCT_MACRO(_1,_2,_3,_4,_5,_6,NAME,...) NAME
#define GEN_STRUCT(...) GET_GEN_STRUCT_MACRO(__VA_ARGS__, GEN_STRUCT5, GEN_STRUCT4, GEN_STRUCT3, GEN_STRUCT2, GEN_STRUCT1)(__VA_ARGS__)

#endif // STRUCT_DECL_H

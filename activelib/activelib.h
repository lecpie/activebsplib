#ifndef ACTIVELIB_H
#define ACTIVELIB_H

#include "pthread.h"

#include "macro_overload.h"
#include "struct_decl.h"

#define ACTIVE_QUEUE_SIZE 512
#define MAX_FUTURE        512

typedef struct active_request
{
    unsigned int future_start;
    unsigned int future_end;

    void (*request_handler) (struct active_request * request_handler);

    void * blob;
} active_request;

typedef struct active_context
{
    pthread_t thr;
    pthread_mutex_t mtx;

    unsigned queue_first_pending;
    unsigned queue_end_pending;

    unsigned run;

    active_request * request_queue;
} active_context;

typedef struct future
{
    void * data;
    unsigned int ready:1;
} future;

unsigned future_next_id = 0;
unsigned future_register_first = 0;

struct future future_table[MAX_FUTURE];

struct future * register_future (void * data)
{
    struct future * next_future = future_table + future_next_id;

    next_future->data  = data;
    next_future->ready = 0;

    if (++future_next_id >= MAX_FUTURE)
    {
        future_next_id = 0;
    }

    return next_future;
}

void * future_get (future * future)
{
    while(! future->ready)
    {
        // active waiting
    }

    return future->data;
}

#define GEN_ACTIVE_ARG(NAME,...) GEN_STRUCT(active_##NAME##_args,__VA_ARGS__)

#define INIT_STRUCT_PARAM(param) args->param = param;
#define INIT_STRUCT_PARAMS1(A1) INIT_STRUCT_PARAM(A1)
#define INIT_STRUCT_PARAMS2(A1,A2) INIT_STRUCT_PARAM(A2) INIT_STRUCT_PARAMS1(A1)
#define INIT_STRUCT_PARAMS3(A1,A2,A3) INIT_STRUCT_PARAM(A3) INIT_STRUCT_PARAMS2(A1,A2)
#define INIT_STRUCT_PARAMS4(A1,A2,A3,A4) INIT_STRUCT_PARAM(A4) INIT_STRUCT_PARAMS3(A1,A2,A3)
#define INIT_STRUCT_PARAMS5(A1,A2,A3,A4,A5) INIT_STRUCT_PARAM(A5) INIT_STRUCT_PARAMS4(A1,A2,A3,A4)

#define GET_INIT_STRUCT_PARAMS_MACRO(_1,_2,_3,_4,_5,NAME,...) NAME
#define INIT_STRUCT_PARAMS(...) GET_INIT_STRUCT_PARAMS_MACRO(__VA_ARGS__, INIT_STRUCT_PARAMS5, INIT_STRUCT_PARAMS4, INIT_STRUCT_PARAMS3, INIT_STRUCT_PARAMS2, INIT_STRUCT_PARAMS1)(__VA_ARGS__)

#define DECL_REQUEST_HANDLER(NAME,...) \
void handle_active_##NAME##_request (active_request * request) \
{ \
    active_##NAME##_args * args = request->blob; \
\
    NAME ACTIVE_CALL_ARG(__VA_ARGS__); \
}

#define GENDECLLIST1(_1) _1 _a1
#define GENDECLLIST2(_1,_2) _1 _a1, _2 _a2
#define GENDECLLIST3(_1,_2,_3) _1 _a1, _2 _a2, _3 _a3
#define GENDECLLIST4(_1,_2,_3,_4)  _1 _a1, _2 _a2, _3 _a3, _4 _a4
#define GENDECLLIST5(_1,_2,_3,_4,_5) _1 _a1, _2 _a2, _3 _a3, _4 _a4, _5 _a5

#define GENARGLIST1(_1) _a1
#define GENARGLIST2(_1,_2) _a1,_a2
#define GENARGLIST3(_1,_2,_3) _a1,_a2,_a3
#define GENARGLIST4(_1,_2,_3,_4) _a1,_a2,_a3,_a4
#define GENARGLIST5(_1,_2,_3,_4,_5) _a1,_a2,_a3,_a4,_a5

#define GET_GENDECLLIST_MACRO(_1,_2,_3,_4,_5,NAME,...) NAME
#define GENDECLLIST(...) GET_GENARGLIST_MACRO(__VA_ARGS__,GENDECLLIST5,GENDECLLIST4,GENDECLLIST3,GENDECLLIST2,GENDECLLIST1)(__VA_ARGS__)

#define GET_GENARGLIST_MACRO(_1,_2,_3,_4,_5,NAME,...) NAME
#define GENARGLIST(...) GET_GENARGLIST_MACRO(__VA_ARGS__,GENARGLIST5,GENARGLIST4,GENARGLIST3,GENARGLIST2,GENARGLIST1)(__VA_ARGS__)

#define ACTIVE_CALL_DECL(NAME,...) \
void active_##NAME (struct active_context * active_context, GENDECLLIST(__VA_ARGS__)) \
{ \
    pthread_mutex_lock(&active_context->mtx); \
\
    unsigned next_pending = active_context->queue_end_pending >= ACTIVE_QUEUE_SIZE ? 0 : active_context->queue_end_pending; \
\
    active_request * request =  ((active_request *) active_context->request_queue) + active_context->queue_end_pending;\
    \
    active_##NAME##_args * args = request->blob = malloc(sizeof(active_##NAME##_args)); \
\
    /* Init request parameters */ \
    INIT_STRUCT_PARAMS(GENARGLIST(__VA_ARGS__)); \
\
    request->request_handler = handle_active_##NAME##_request; \
\
    /* Claim futures */ \
    request->future_start = future_register_first; \
    request->future_end   = future_next_id; \
    future_register_first = future_next_id; \
\
    active_context->queue_end_pending = next_pending + 1; \
\
    pthread_mutex_unlock(&active_context->mtx); \
}

#define ACTIVE_CALL_ARG1(ARG1) (args->ARG1)
#define ACTIVE_CALL_ARG2(ARG1,ARG2) (args->ARG1, args->ARG2)
#define ACTIVE_CALL_ARG3(ARG1,ARG2,ARG3) (args->ARG1, args->ARG2, args->ARG3)
#define ACTIVE_CALL_ARG4(ARG1,ARG2,ARG3,ARG4) (args->ARG1, args->ARG2, args->ARG3, args->ARG4)
#define ACTIVE_CALL_ARG5(ARG1,ARG2,ARG3,ARG4,ARG5) (args->ARG1, args->ARG2, args->ARG3, args->ARG4, args->ARG5)

#define GET_ACTIVE_CALL_ARG_MACRO(_1,_2,_3,_4,_5,NAME,...) NAME
#define ACTIVE_CALL_ARG(...) GET_ACTIVE_CALL_ARG_MACRO(__VA_ARGS__, ACTIVE_CALL_ARG5, ACTIVE_CALL_ARG4, ACTIVE_CALL_ARG3, ACTIVE_CALL_ARG2, ACTIVE_CALL_ARG1)(__VA_ARGS__)

void * active_thread (void * arg)
{
    struct active_context * active_context = arg;

    while (active_context->run)
    { \
        pthread_mutex_lock(&active_context->mtx);
\
        /* Try again if there is no pending request, active waiting*/
        if (active_context->queue_first_pending == active_context->queue_end_pending)
        {
            pthread_mutex_unlock(&active_context->mtx);

            continue;
        } \
        /* Maybe do not use lock for read access ? */
        /* As long as no two thread are on the same context, if the above are modified, it means a request is going to arrive */
        /* so we only need to protect write accesses as the lock will wait other writes to finish to have coherent states     */

        /* Find next request slot */
        active_request * request = active_context->request_queue + active_context->queue_first_pending; \

        /* Ajust circular buffers */ \
        if (active_context->queue_first_pending++ >= ACTIVE_QUEUE_SIZE)
        {
            active_context->queue_first_pending = 0;

            if (active_context->queue_end_pending >= ACTIVE_QUEUE_SIZE)
            {
                active_context->queue_end_pending = 0;
            }
        }

        pthread_mutex_unlock(&active_context->mtx);

        request->request_handler(request);

        unsigned i = request->future_start;

        while (i != request->future_end)
        {
            future_table[i].ready = 1;

            if (++i >= MAX_FUTURE)
            {
                i = 0;
            }
        }
    }
}

#define ACTIVE_CALL(name) active_##name

#define ACTIVE_FUNCTION_DECL(NAME,...) \
    GEN_ACTIVE_ARG(NAME,GENDECLLIST(__VA_ARGS__)) \
    DECL_REQUEST_HANDLER(NAME,GENARGLIST(__VA_ARGS__)) \
    ACTIVE_CALL_DECL(NAME,__VA_ARGS__)

void active_start (active_context * active_context)
{
    active_context->request_queue = malloc(ACTIVE_QUEUE_SIZE * sizeof(struct active_request));

    active_context->queue_first_pending = 0;
    active_context->queue_end_pending   = 0;

    active_context->run = 1;

    pthread_mutex_init(&active_context->mtx, NULL);

    pthread_create(&active_context->thr, NULL, active_thread, active_context);
}

void active_stop(active_context * active_context)
{
    free(active_context->request_queue);

    active_context->run = 0;
    pthread_join(active_context->thr,NULL);
}

#endif // ACTIVELIB_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <pthread.h>

#define ACTIVE_QUEUE_SIZE 512
#define MAX_FUTURE        512

typedef struct active_request
{
    unsigned int future_start;
    unsigned int future_end;

    void (*request_handler) (struct active_request * request);

    void * blob;
} active_request;

typedef struct active_context
{
    pthread_t thr;
    pthread_mutex_t mtx;

    unsigned queue_first_pending;
    unsigned queue_end_pending;

    unsigned run;

    active_request * request_queue;
} active_context;

struct future
{
    void * data;
    unsigned int ready:1;
};

unsigned future_next_id = 0;
unsigned future_register_first = 0;

struct future future_table[MAX_FUTURE];

struct future * register_future (void * data)
{
    struct future * next_future = future_table + future_next_id;

    next_future->data  = data;
    next_future->ready = 0;

    if (++future_next_id >= MAX_FUTURE)
    {
        future_next_id = 0;
    }

    return next_future;
}

void * future_get (struct future * future)
{
    while(! future->ready)
    {
        // active waiting
    }

    return future->data;
}

void hello (char * str, const char * who)
{
    sprintf(str, "Hello %s", who);
    fflush(stdout);
}

void safe_hello (char * str, const char * who, int n)
{
    snprintf(str, n, "Hello %s", who);
    fflush(stdout);
}

//TODO varg trick

typedef struct active_hello_args
{
    char * str;
    const char * who;
} active_hello_args;


typedef struct active_safe_hello_args
{
    char * str;
    const char * who;
    int n;
} active_safe_hello_args;

void handle_active_hello_request (active_request * request)
{
    active_hello_args * args = request->blob;

    hello(args->str, args->who);
}

void handle_active_safe_hello_request (active_request * request)
{
    active_safe_hello_args * args = request->blob;

    safe_hello(args->str, args->who, args->n);
}

void active_hello (struct active_context * active_context, char * str, const char * who)
{
    pthread_mutex_lock(&active_context->mtx);

    unsigned next_pending = active_context->queue_end_pending >= ACTIVE_QUEUE_SIZE ? 0 : active_context->queue_end_pending;

    active_request * request =  ((active_request *) active_context->request_queue) + active_context->queue_end_pending;

    request->request_handler = handle_active_hello_request;

    active_hello_args * args = request->blob = malloc(sizeof(active_hello_args));

    /* Init request parameters */
    args->str = str;
    args->who = who;

    /* Claim futures */
    request->future_start = future_register_first;
    request->future_end   = future_next_id;
    future_register_first = future_next_id;

    active_context->queue_end_pending = next_pending + 1;

    pthread_mutex_unlock(&active_context->mtx);
}

void active_safe_hello (struct active_context * active_context, char * str, const char * who, int n)
{
    pthread_mutex_lock(&active_context->mtx);

    unsigned next_pending = active_context->queue_end_pending >= ACTIVE_QUEUE_SIZE ? 0 : active_context->queue_end_pending;

    active_request * request =  ((active_request *) active_context->request_queue) + active_context->queue_end_pending;

    request->request_handler = handle_active_safe_hello_request;

    active_safe_hello_args * args = request->blob = malloc(sizeof(active_safe_hello_args));

    /* Init request parameters */
    args->str = str;
    args->who = who;
    args->n   = n;

    /* Claim futures */
    request->future_start = future_register_first;
    request->future_end   = future_next_id;
    future_register_first = future_next_id;

    active_context->queue_end_pending = next_pending + 1;

    pthread_mutex_unlock(&active_context->mtx);
}

void * active_obj_thread (void * arg)
{
    struct active_context * active_context = arg;

    while (active_context->run)
    {
        pthread_mutex_lock(&active_context->mtx);

        /* Try again if there is no pending request, active waiting*/
        if (active_context->queue_first_pending == active_context->queue_end_pending)
        {
            pthread_mutex_unlock(&active_context->mtx);

            continue;
        }
        /* As long as no two thread are on the same context, if the above are modified, it means a request is going to arrive */
        /* so we only need to protect write accesses as the lock will wait other writes to finish to have coherent states     */

        /* Find next request slot */
        active_request * request = active_context->request_queue + active_context->queue_first_pending;

        /* Ajust circular buffers */
        if (active_context->queue_first_pending++ >= ACTIVE_QUEUE_SIZE)
        {
            active_context->queue_first_pending = 0;

            if (active_context->queue_end_pending >= ACTIVE_QUEUE_SIZE)
            {
                active_context->queue_end_pending = 0;
            }
        }

        pthread_mutex_unlock(&active_context->mtx);

        request->request_handler(request);

        free(request->blob);

        unsigned i = request->future_start;

        while (i != request->future_end)
        {
            future_table[i].ready = 1;

            if (++i >= MAX_FUTURE)
            {
                i = 0;
            }
        }
    }
}

void active_context_start (active_context * active_context, void * (*active_thr_f) (void *))
{
    // TODO : untie from active_hello
    // Problem : how to handle different request types with allocation ?

    active_context->request_queue = malloc(ACTIVE_QUEUE_SIZE * sizeof(active_request));

    active_context->queue_first_pending = 0;
    active_context->queue_end_pending   = 0;

    active_context->run = 1;

    pthread_mutex_init(&active_context->mtx, NULL);

    pthread_create(&active_context->thr, NULL, active_thr_f, active_context);
}

void active_context_stop(active_context * active_context)
{
    free(active_context->request_queue);

    active_context->run = 0;
    pthread_join(active_context->thr,NULL);
}

#define ACTIVE_CALL(NAME, CONTEXT, ...) \
    active_ ## NAME (CONTEXT, handle_active_ ## NAME ## _request, __VA_ARGS__);

int main(int argc, char **argv)
{
    // init active functions

    active_context active1;
    active_context active2;

    active_context_start(&active1,active_obj_thread);
    active_context_start(&active2,active_obj_thread);

    char buf1[16];
    char buf2[16];
    char buf3[16];

    struct future * future1;
    struct future * future3;

    active_hello(&active1, (future1 = register_future(buf1))->data, "A");

    struct future * future2 = register_future(buf2);
    active_hello(&active2, buf2, "B");

    future3 = register_future(buf3);

    //ACTIVE_CALL(safe_hello, &active1, buf3, "C", 16);
    active_safe_hello(&active1, buf3, "C", 16);

    // example without cast is cheating, printf %s is doing the char * cast
    printf("%s\n", (char *) future_get(future1));
    printf("%s\n",          future_get(future2));
    printf("%s\n",          future_get(future3));

    active_context_stop(&active1);
    active_context_stop(&active2);

    return 0;
}



#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <pthread.h>

#include "activelib.h"

void hello (char * str, const char * who)
{
    sprintf(str, "Hello %s", who);
    fflush(stdout);
}

void safe_hello (char * str, const char * who, int n)
{
    snprintf(str, n, "Hello %s", who);
    fflush(stdout);
}

// Declare active functions
ACTIVE_FUNCTION_DECL(hello,      char *, const char *)
ACTIVE_FUNCTION_DECL(safe_hello, char *, const char *, int)

// Some shortcut for future as parameter (move to lib ?)
#define MAP_FUTURE(FUTURE,DATA) (FUTURE = register_future(DATA))->data
#define FUTURE(FUTURE) FUTURE->data

#define STRBUFSZ 16

int main(int argc, char **argv)
{
    // init active contexes

    active_context active1;
    active_context active2;

    active_start (&active1);
    active_start (&active2);

    char buf1[STRBUFSZ];
    char buf2[STRBUFSZ];
    char buf3[STRBUFSZ];
    char buf4[STRBUFSZ];
    char buf5[STRBUFSZ];

    future * future1,
           * future2,
           * future3,
           * future4,
           * future5;

    // Different call syntaxes

    future1 = register_future(buf1);
    active_hello(&active1, buf1, "A");

    active_hello(&active1, (future2 = register_future(buf2))->data, "B");

    future3 = register_future(buf3);
    ACTIVE_CALL(safe_hello)(&active1, buf3, "C", STRBUFSZ);

    future4 = register_future(buf4);
    ACTIVE_CALL(safe_hello)(&active2, FUTURE(future4), "D", STRBUFSZ);

    ACTIVE_CALL(safe_hello)(&active2, MAP_FUTURE(future5,buf5), "E", STRBUFSZ);

    // example without cast is cheating, printf %s is doing the char * cast
    printf("%s\n", (char *) future_get(future1));

    printf("%s\n", future_get(future2));
    printf("%s\n", future_get(future3));
    printf("%s\n", future_get(future4));
    printf("%s\n", future_get(future5));

    active_stop(&active1);
    active_stop(&active2);

    return 0;
}

